Proyecto del curso “Universidad Angular 2021, de cero a experto” (UDEMY)

Aplicación desarrollada para control de clientes y saldos, administrando un listado de personas con sus datos principales.

Cliente creado con Angular CLI versión 11, separando sus vistas y componentes.

El objetivo de este ejercicio fue estudiar las bases de Angular, y el concepto de SPA (Single Page Application).
Así como también conocer el manejo de los diferentes componentes y elementos de cada componente (archivos .ts y .html).
Uso de conceptos como Property Binding y Two Way Binding , así como interpolación cuando se trabaja con atributos de la clase.
Manejo de inyección de dependencias, y de directivas *ngFor y *ngIf .

Base de datos utilizada: Firebase de Google (en este caso, Real Time Database).
La clase "DataService" es para la configuración de conexión hacia la base de datos (mediante métodos GET, PUT y DELETE).
La clase PersonasService es para administrar el arreglo local de Personas (es decir, del lado del cliente) y va actualizándose
en base a los cambios ocurridos en la base de datos.

Incluye archivo "app-routing.module.ts" para configurar las rutas de navegación, así como los guardianes de acceso a cada ruta.
Con validación de acceso a la aplicación (usuario y contraseña).

